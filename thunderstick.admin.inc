<?php

/**
 * @file
 * Bring on the thunder.
 */

/**
 * Form callback; button to start up the batch api.
 */
function thunderstick_form() {
  $form = array();

  $form['thundertwins_activate'] = array(
    '#type' => 'submit',
    '#value' => t("Don't click me!"),
  );

  return $form;
}

function thunderstick_form_submit($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];

  if ($op == t("Don't click me!")) {
    $batch = array(
      'operations' => array(
        array('thunderstick_batch_process'),
      ),
      'finished' => 'thunderstick_batch_finished',
      'title' => t('Bringing the thunder!'),
      'init_message' => t('Starting up processing...'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('Danger Will Robinson!'),
      'file' => drupal_get_path('module', 'thunderstick') . '/thunderstick.admin.inc',
    );
    batch_set($batch);
  }
}

/**
 * Batch API callback to rebuild a table.
 */
function thunderstick_batch_process(&$context) {

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = db_result(db_query("SELECT COUNT(DISTINCT nid) FROM {node}"));
    // If you only want to act on a certain node type the query would look like.
    // SELECT COUNT(DISTINCT nid) FROM {node} WHERE type = 'foo'
  }

  // For this example, we decide that we can safely process
  // 20 nodes at a time without a timeout.
  $limit = 20;

  // Get the next round of nodes wor act on.
  $result = db_query_range("SELECT nid FROM {node} WHERE nid > %d ORDER BY nid ASC", $context['sandbox']['current_node'], 0, $limit);
  
  // If you only want to act on a certain node type the query would look like.
  // SELECT nid FROM {node} WHERE type = 'foo' AND nid > %d ORDER BY nid ASC
  
  while ($row = db_fetch_array($result)) {
    $node = node_load($row['nid'], NULL, TRUE);
    
    
    // Act on the node and maybe save it when done.
    

    // If we track all the titles we process the arrary will exceed
    // the max mysql transaction size. Just storing a count.
    $context['results'] = empty($context['results']) ? 1 : $context['results'] + 1;

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $node->nid;
    $context['message'] = t('Now processing %node', array('%node' => $node->title));
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function thunderstick_batch_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = $results .' processed.';
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
  }
  drupal_set_message($message);
}